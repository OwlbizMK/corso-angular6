import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Vehiculo } from '../models/vehiculos.model';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {

  @Input() vehiculo: Vehiculo;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit(): void {
  }

}
