import { Component, OnInit } from '@angular/core';
import { Vehiculo } from '../models/vehiculos.model';

@Component({
  selector: 'app-lista-vehiculos',
  templateUrl: './lista-vehiculos.component.html',
  styleUrls: ['./lista-vehiculos.component.css']
})
export class ListaVehiculosComponent implements OnInit {
  vehiculos: Vehiculo[];
  constructor() {
    this.vehiculos = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string, descripcion:string):boolean {
    this.vehiculos.push(new Vehiculo(nombre,url,descripcion));
    console.log(this.vehiculos);
    return false;
  }
}
